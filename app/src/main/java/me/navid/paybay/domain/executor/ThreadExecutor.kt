package me.navid.paybay.domain.executor

import java.util.concurrent.Executor

/**
 * Created by navid.eghbali on 4/8/19.
 */

interface ThreadExecutor : Executor