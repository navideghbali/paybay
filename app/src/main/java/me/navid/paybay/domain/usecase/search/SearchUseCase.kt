package me.navid.paybay.domain.usecase.search

import androidx.lifecycle.LiveData
import io.reactivex.schedulers.Schedulers
import me.navid.paybay.domain.entity.PhotoObject
import me.navid.paybay.domain.executor.PostExecutionThread
import me.navid.paybay.domain.executor.ThreadExecutor
import me.navid.paybay.domain.repository.SearchRepository
import me.navid.paybay.domain.usecase.UseCase
import me.navid.paybay.result.Result
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/6/19.
 *
 * Performs a search from a query string.
 * A list of hits is returned by the Pixabay.com API.
 */

open class SearchUseCase @Inject constructor(
    private val repository: SearchRepository,
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) : UseCase<String, List<PhotoObject>>() {
    override fun execute(parameters: String): LiveData<Result<List<PhotoObject>>> {
        val query = parameters.toLowerCase()
        result.postValue(Result.Loading)

        repository.getHits(query)
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.scheduler())
            .subscribe({ result.postValue(Result.Success(it)) }, {
                it.printStackTrace()
                result.postValue(Result.Error(Exception(it)))
            })

        return result
    }
}
