package me.navid.paybay.domain.entity

import java.io.Serializable

/**
 * Created by navid.eghbali on 4/6/19.
 *
 * Describes a photo to be used in presentation layer.
 */

data class PhotoObject(
    val id: Long,
    val pageUrl: String,
    val tags: String,
    val thumbnailUrl: String,
    val thumbnailWidth:Int,
    val thumbnailHeight: Int,
    val originalUrl: String,
    val favorites: Int,
    val likes: Int,
    val comments: Int,
    val user: String,
    val avatarUrl: String
) : Serializable