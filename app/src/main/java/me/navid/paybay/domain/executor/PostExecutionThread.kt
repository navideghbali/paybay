package me.navid.paybay.domain.executor

import io.reactivex.Scheduler

/**
 * Created by navid.eghbali on 4/8/19.
 */

interface PostExecutionThread {
    fun scheduler(): Scheduler
}