package me.navid.paybay.domain.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import me.navid.paybay.result.Result

/**
 * Created by navid.eghbali on 4/6/19.
 *
 * Executes business logic in its execute method & keep posting updates to the result.
 */

abstract class UseCase<in P, R> {
    protected val result = MutableLiveData<Result<R>>()

    abstract fun execute(parameters: P): LiveData<Result<R>>
}