package me.navid.paybay.domain.repository

import io.reactivex.Single
import me.navid.paybay.domain.entity.PhotoObject

/**
 * Created by navid.eghbali on 4/6/19.
 */

interface SearchRepository {
    fun getHits(query: String): Single<List<PhotoObject>>
}
