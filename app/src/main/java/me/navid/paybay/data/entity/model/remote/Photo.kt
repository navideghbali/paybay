package me.navid.paybay.data.entity.model.remote

import com.google.gson.annotations.SerializedName

/**
 * Created by navid.eghbali on 4/3/19.
 *
 * Describes a hit which is returned from the API.
 */

data class Photo(
    val id: Long,
    @SerializedName("pageURL")
    val pageUrl: String,
    val type: String,
    val tags: String,
    @SerializedName("previewURL")
    val previewUrl: String,
    val previewWidth: Int,
    val previewHeight: Int,
    @SerializedName("largeImageURL")
    val largeImageUrl: String,
    val views: Long,
    val downloads: Long,
    val favorites: Int,
    val likes: Int,
    val comments: Int,
    @SerializedName("user_id")
    val userId: Long,
    val user: String,
    @SerializedName("userImageURL")
    val userImageUrl: String
)
