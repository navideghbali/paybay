package me.navid.paybay.data.repository.search

import io.reactivex.Single
import me.navid.paybay.data.entity.model.remote.Photo
import me.navid.paybay.data.network.SearchApi
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/6/19.
 */

class SmartSearchDataSource @Inject constructor(
    private val searchApi: SearchApi
) : SearchDataSource {
    override fun getHits(query: String): Single<List<Photo>> {
        return searchApi.search(query = query)
            .map { it.hits }
    }
}
