package me.navid.paybay.data.network

import io.reactivex.Single
import me.navid.paybay.BuildConfig
import me.navid.paybay.data.entity.model.remote.Search
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by navid.eghbali on 4/3/19.
 */

interface SearchApi {
    @GET("api")
    fun search(
        @Query(value = "key") key: String = BuildConfig.API_KEY,
        @Query(value = "image_type") imageType: String = "photo",
        @Query(value = "q", encoded = true) query: String
    ): Single<Search.Response>
}
