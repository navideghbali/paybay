package me.navid.paybay.data.entity.model.remote

/**
 * Created by navid.eghbali on 4/3/19.
 *
 * Describes the search response which is returned from the API.
 */

sealed class Search {
    data class Response(
        val total: Long,
        val totalHits: Long,
        val hits: List<Photo>
    ) : Search()
}
