package me.navid.paybay.data.repository.search

import io.reactivex.Single
import me.navid.paybay.data.entity.model.remote.Photo

/**
 * Created by navid.eghbali on 4/6/19.
 */

interface SearchDataSource {
    fun getHits(query: String): Single<List<Photo>>
}
