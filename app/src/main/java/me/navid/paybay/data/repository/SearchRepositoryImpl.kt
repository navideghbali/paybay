package me.navid.paybay.data.repository

import io.reactivex.Single
import me.navid.paybay.data.entity.mapToPhotoObjectList
import me.navid.paybay.data.repository.search.SmartSearchDataSource
import me.navid.paybay.domain.entity.PhotoObject
import me.navid.paybay.domain.repository.SearchRepository
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/6/19.
 *
 * Single point of access to search for the presentation layer.
 */

class SearchRepositoryImpl @Inject constructor(
    private val searchDataSource: SmartSearchDataSource
) : SearchRepository {
    override fun getHits(query: String): Single<List<PhotoObject>> =
        searchDataSource.getHits(query).map { it.mapToPhotoObjectList() }
}
