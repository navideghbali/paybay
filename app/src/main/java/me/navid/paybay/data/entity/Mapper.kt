package me.navid.paybay.data.entity

import me.navid.paybay.data.entity.model.remote.Photo
import me.navid.paybay.domain.entity.PhotoObject

/**
 * Created by navid.eghbali on 4/6/19.
 */

fun Photo.map(): PhotoObject = PhotoObject(
    id,
    pageUrl,
    tags,
    previewUrl,
    previewWidth,
    previewHeight,
    largeImageUrl,
    favorites,
    likes,
    comments,
    user,
    userImageUrl
)

fun List<Photo>.mapToPhotoObjectList(): List<PhotoObject> = map { it.map() }
