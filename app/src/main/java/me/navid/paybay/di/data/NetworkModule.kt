package me.navid.paybay.di.data

import dagger.Module
import dagger.Provides
import dagger.Reusable
import me.navid.paybay.data.network.NetworkFactory
import me.navid.paybay.data.network.SearchApi

/**
 * Created by navid.eghbali on 4/3/19.
 */

@Module
class NetworkModule {
    @Provides
    @Reusable
    fun searchApi(networkFactory: NetworkFactory) =
        networkFactory.create(SearchApi::class.java)
}
