package me.navid.paybay.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import me.navid.paybay.presentation.ui.MainActivity
import me.navid.paybay.presentation.ui.MainActivityModule

/**
 * Created by navid.eghbali on 4/6/19.
 */

@Module
abstract class AppModule {
    @PerActivity
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity
}
