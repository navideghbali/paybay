package me.navid.paybay.di

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module

/**
 * Created by navid.eghbali on 4/3/19.
 */

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: PayBayViewModelFactory): ViewModelProvider.Factory
}
