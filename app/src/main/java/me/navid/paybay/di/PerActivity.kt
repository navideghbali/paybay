package me.navid.paybay.di

import javax.inject.Scope

/**
 * Created by navid.eghbali on 4/3/19.
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity
