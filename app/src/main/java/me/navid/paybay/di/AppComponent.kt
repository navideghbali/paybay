package me.navid.paybay.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import me.navid.paybay.MainApplication
import me.navid.paybay.di.data.NetworkModule
import me.navid.paybay.di.data.RepositoryModule
import me.navid.paybay.di.domain.ExecutionModule
import javax.inject.Singleton

/**
 * Created by navid.eghbali on 4/3/19.
 */

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ExecutionModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent : AndroidInjector<MainApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MainApplication>()
}
