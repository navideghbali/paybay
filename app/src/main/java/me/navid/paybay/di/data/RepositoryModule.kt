package me.navid.paybay.di.data

import dagger.Binds
import dagger.Module
import me.navid.paybay.data.repository.SearchRepositoryImpl
import me.navid.paybay.domain.repository.SearchRepository

/**
 * Created by navid.eghbali on 4/6/19.
 */

@Module
abstract class RepositoryModule {
    @Binds
    abstract fun searchRepository(implementation: SearchRepositoryImpl): SearchRepository
}
