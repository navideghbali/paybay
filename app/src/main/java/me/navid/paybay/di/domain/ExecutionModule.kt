package me.navid.paybay.di.domain

import dagger.Binds
import dagger.Module
import me.navid.paybay.data.executor.JobExecutor
import me.navid.paybay.domain.executor.PostExecutionThread
import me.navid.paybay.domain.executor.ThreadExecutor
import me.navid.paybay.presentation.executor.UiThread

/**
 * Created by navid.eghbali on 4/8/19.
 */

@Module
abstract class ExecutionModule {
    @Binds
    abstract fun threadExecutor(jobExecutor: JobExecutor): ThreadExecutor

    @Binds
    abstract fun postExecutionThread(uiThread: UiThread): PostExecutionThread
}