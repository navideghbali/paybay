package me.navid.paybay.presentation.ui.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_explore.*
import me.navid.paybay.R
import me.navid.paybay.presentation.extension.hideKeyboard
import me.navid.paybay.presentation.extension.observe
import me.navid.paybay.presentation.extension.setSwipeRefreshColors
import me.navid.paybay.presentation.extension.viewModelProvider
import me.navid.paybay.presentation.ui.MainNavigator
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/3/19.
 */

class ExploreFragment : DaggerFragment() {
    @Inject
    lateinit var navigator: MainNavigator
    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var hitsAdapter: HitsAdapter
    private lateinit var viewModel: ExploreViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(factory)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_explore, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
        observe(viewModel.query) { input.setText(it) }
        observe(viewModel.hits) { hitsAdapter.addAll(it) }
        observe(viewModel.isLoading) { swipeLayout.isRefreshing = it }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        input.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    view?.hideKeyboard()
                    viewModel.search(input.text.toString())
                    true
                }
                else -> false
            }
        }
    }

    private fun initUI() {
        hitsAdapter = HitsAdapter { navigator.onHitClicked(it) }
        with(list) {
            layoutManager = GridLayoutManager(requireContext(), resources.getInteger(R.integer.span_count))
            adapter = hitsAdapter
            setHasFixedSize(true)
        }
        with(swipeLayout) {
            isEnabled = false
            setSwipeRefreshColors(resources.getIntArray(R.array.swipe_refresh))
        }
    }
}
