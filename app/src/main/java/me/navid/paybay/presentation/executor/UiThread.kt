package me.navid.paybay.presentation.executor

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import me.navid.paybay.domain.executor.PostExecutionThread
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/8/19.
 */

class UiThread @Inject constructor() : PostExecutionThread {
    override fun scheduler(): Scheduler = AndroidSchedulers.mainThread()
}
