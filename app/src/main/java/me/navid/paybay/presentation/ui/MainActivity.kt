package me.navid.paybay.presentation.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.navigation.findNavController
import dagger.android.support.DaggerAppCompatActivity
import me.navid.paybay.R
import me.navid.paybay.domain.entity.PhotoObject
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/2/19.
 */

class MainActivity : DaggerAppCompatActivity(), MainNavigator {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onSupportNavigateUp() = findNavController(R.id.nav_host_main).navigateUp()

    override fun onHitClicked(item: PhotoObject) {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.details_prompt))
            .setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                findNavController(R.id.nav_host_main).navigate(
                    R.id.action_exploreFragment_to_detailFragment,
                    Bundle().apply { putSerializable("Photo", item) }
                )
                dialog.dismiss()
            }
            .setNegativeButton(getString(R.string.no)) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }

    override fun onBack() {
        findNavController(R.id.nav_host_main).navigateUp()
    }

    override fun onExploreClicked(url: String?) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }
}
