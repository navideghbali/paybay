package me.navid.paybay.presentation.ui

import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import me.navid.paybay.di.PerActivity
import me.navid.paybay.di.PerFragment
import me.navid.paybay.presentation.ui.detail.DetailFragment
import me.navid.paybay.presentation.ui.detail.DetailFragmentModule
import me.navid.paybay.presentation.ui.explore.ExploreFragment
import me.navid.paybay.presentation.ui.explore.ExploreFragmentModule

/**
 * Created by navid.eghbali on 4/3/19.
 */

@Module
abstract class MainActivityModule {
    @PerFragment
    @ContributesAndroidInjector(modules = [DetailFragmentModule::class])
    internal abstract fun detailFragment(): DetailFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [ExploreFragmentModule::class])
    internal abstract fun exploreFragment(): ExploreFragment

    @Binds
    @PerActivity
    internal abstract fun mainNavigator(mainActivity: MainActivity): MainNavigator
}
