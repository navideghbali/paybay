package me.navid.paybay.presentation.ui.detail

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import me.navid.paybay.di.ViewModelKey

/**
 * Created by navid.eghbali on 4/4/19.
 */

@Module
internal abstract class DetailFragmentModule {
    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun detailViewModel(viewModel: DetailViewModel): ViewModel
}
