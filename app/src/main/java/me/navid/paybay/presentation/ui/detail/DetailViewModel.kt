package me.navid.paybay.presentation.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.navid.paybay.domain.entity.PhotoObject
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/4/19.
 */

class DetailViewModel @Inject constructor() : ViewModel() {
    val photo = MutableLiveData<PhotoObject>()

    fun setModel(model: PhotoObject?) {
        photo.value = model
    }

    fun getPageUrl() = photo.value?.pageUrl
}
