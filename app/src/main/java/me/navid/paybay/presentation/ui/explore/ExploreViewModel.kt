package me.navid.paybay.presentation.ui.explore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.navid.paybay.domain.entity.PhotoObject
import me.navid.paybay.domain.usecase.search.SearchUseCase
import me.navid.paybay.presentation.extension.map
import me.navid.paybay.presentation.extension.switchMap
import me.navid.paybay.result.Result
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/4/19.
 */

class ExploreViewModel @Inject constructor(
    searchUseCase: SearchUseCase
) : ViewModel() {
    private val searchResults: LiveData<Result<List<PhotoObject>>>

    val isLoading: LiveData<Boolean>
    val query = MutableLiveData<String>()
    val hits: LiveData<List<PhotoObject>>

    init {
        searchResults = query.switchMap { searchUseCase.execute(it) }
        isLoading = searchResults.map { it is Result.Loading }
        hits = searchResults.map {
            if (it is Result.Success)
                it.data
            else
                emptyList()
        }

        search("fruits")
    }

    fun search(term: String) {
        query.value = term
    }
}
