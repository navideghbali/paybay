package me.navid.paybay.presentation.ui

import me.navid.paybay.domain.entity.PhotoObject

/**
 * Created by navid.eghbali on 4/5/19.
 */

interface MainNavigator {
    fun onHitClicked(item: PhotoObject)
    fun onBack()
    fun onExploreClicked(url: String?)
}
