package me.navid.paybay.presentation.ui.detail

import android.graphics.drawable.Drawable
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import me.navid.paybay.presentation.extension.hide

/**
 * Created by navid.eghbali on 4/8/19.
 */

@BindingAdapter("imageUrl", "placeholder")
fun loadImage(view: AppCompatImageView, imageUrl: String, placeholder: Drawable) {
    Glide.with(view.context)
        .load(imageUrl)
        .placeholder(placeholder)
        .apply(RequestOptions().transform(CircleCrop()))
        .into(view)
}

@BindingAdapter("imageUrl", "thumbnailUrl", "progress")
fun loadImage(view: AppCompatImageView, imageUrl: String, thumbnailUrl: String, progress: View) {
    Glide.with(view.context)
        .load(imageUrl)
        .thumbnail(Glide.with(view.context).load(thumbnailUrl))
        .listener(object : RequestListener<Drawable> {
            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                progress.hide()
                return false
            }

            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                progress.hide()
                return false
            }
        })
        .into(view)
}
