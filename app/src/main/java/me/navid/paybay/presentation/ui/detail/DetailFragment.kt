package me.navid.paybay.presentation.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detail.*
import me.navid.paybay.R
import me.navid.paybay.databinding.FragmentDetailBinding
import me.navid.paybay.domain.entity.PhotoObject
import me.navid.paybay.presentation.extension.viewModelProvider
import me.navid.paybay.presentation.ui.MainNavigator
import javax.inject.Inject

/**
 * Created by navid.eghbali on 4/3/19.
 */

class DetailFragment : DaggerFragment() {
    @Inject
    lateinit var navigator: MainNavigator
    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private lateinit var viewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = viewModelProvider(factory)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentDetailBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = this@DetailFragment
        }
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let { viewModel.setModel(it["Photo"] as? PhotoObject) }
    }

    private fun initUI() {
        with(toolbar) {
            setNavigationOnClickListener { navigator.onBack() }
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_explore -> {
                        navigator.onExploreClicked(viewModel.getPageUrl())
                        true
                    }
                    else -> false
                }
            }
        }
    }
}
