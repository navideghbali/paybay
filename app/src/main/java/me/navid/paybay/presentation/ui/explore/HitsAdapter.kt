package me.navid.paybay.presentation.ui.explore

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_hit.*
import me.navid.paybay.R
import me.navid.paybay.domain.entity.PhotoObject
import me.navid.paybay.presentation.extension.inflate
import me.navid.paybay.presentation.extension.load

/**
 * Created by navid.eghbali on 4/4/19.
 */

class HitsAdapter(private val clickListener: (PhotoObject) -> Unit) : RecyclerView.Adapter<HitViewHolder>() {
    private var items = ArrayList<PhotoObject>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        HitViewHolder(parent.inflate(R.layout.item_hit), clickListener)

    override fun onBindViewHolder(holder: HitViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    fun addAll(photos: List<PhotoObject>) {
        items = ArrayList(photos)
        notifyDataSetChanged()
    }

    fun clear() {
        items = ArrayList()
        notifyDataSetChanged()
    }
}

class HitViewHolder(override val containerView: View, private val clickListener: (PhotoObject) -> Unit) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(item: PhotoObject) {
        Glide.with(thumbnailImage.context)
            .load(item.thumbnailUrl)
            .placeholder(R.drawable.ic_image_placeholder_64dp)
            .override(item.thumbnailWidth, item.thumbnailHeight)
            .into(thumbnailImage)
        avatarImage.load(item.avatarUrl, CircleCrop(), R.drawable.ic_avatar_placeholder_48dp)
        usernameText.text = item.user
        tagsText.text = item.tags

        cardView.setOnClickListener { clickListener(item) }
    }
}
