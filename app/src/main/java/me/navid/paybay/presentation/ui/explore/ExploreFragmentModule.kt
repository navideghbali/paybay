package me.navid.paybay.presentation.ui.explore

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import me.navid.paybay.di.ViewModelKey

/**
 * Created by navid.eghbali on 4/4/19.
 */

@Module
internal abstract class ExploreFragmentModule {
    @Binds
    @IntoMap
    @ViewModelKey(ExploreViewModel::class)
    abstract fun exploreViewModel(viewModel: ExploreViewModel): ViewModel
}