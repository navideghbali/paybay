package me.navid.paybay.presentation.extension

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.annotation.LayoutRes
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.Transformation
import com.bumptech.glide.request.RequestOptions

/**
 * Created by navid.eghbali on 4/4/19.
 */

fun ViewGroup.inflate(@LayoutRes layout: Int, attachToRoot: Boolean = false) =
    LayoutInflater.from(context).inflate(layout, this, attachToRoot)

internal fun ImageView.load(
    url: String,
    @DrawableRes placeholderResourceId: Int
) {
    Glide.with(context)
        .load(url)
        .placeholder(placeholderResourceId)
        .into(this)
}

internal fun ImageView.load(
    url: String,
    transformation: Transformation<Bitmap>,
    @DrawableRes placeholderResourceId: Int
) {
    Glide.with(context)
        .load(url)
        .placeholder(placeholderResourceId)
        .apply(RequestOptions().transform(transformation))
        .into(this)
}

fun SwipeRefreshLayout.setSwipeRefreshColors(colorResIds: IntArray) {
    setColorSchemeColors(*colorResIds)
}

fun View.hideKeyboard() {
    val inputMethodService = context.getSystemService(Context.INPUT_METHOD_SERVICE)
    (inputMethodService as? InputMethodManager)?.hideSoftInputFromWindow(
        windowToken,
        InputMethodManager.HIDE_NOT_ALWAYS
    )
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}
