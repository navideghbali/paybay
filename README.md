# PayBay
PayBay is an android app that uses Pixabay public API.
## Features
The app lets the user to search for images entering one or more words in a textfield. A list of hits is shown and the user can see the image with higher resolution upon clicking on it.
<br/>
For each image the app displays info about the Pixabay user, a list of image's tags, the number of likes, the number of favorites and the number of comments.
## Development Environment
The app is written entirely in Kotlin and uses the Gradle build system.
## Architecture
Followed the recommendations laid out in the [Guide to App Architecture](https://developer.android.com/jetpack/docs/guide). I kept logic away from Activities and Fragments and moved it to ViewModels. I observed data using LiveData and used the Data Binding Library to bind UI components in layouts to the app's data sources. I used a Repository layer for handling data operations.
<br/>
<br/>
I implemented a lightweight domain layer, which sits between the data layer and the presentation layer, and handles discrete pieces of business logic off the UI thread. See the .\*UseCase.kt files under app/domain/usecase for [examples](https://github.com/navid-eghbali/paybay/search?q=UseCase&unscoped_q=UseCase).
<br/>
<br/>
I used [Dagger2](https://google.github.io/dagger/) for dependency injection, [RxJava2](https://github.com/ReactiveX/RxJava), [Retrofit2](https://square.github.io/retrofit/), [Data Binding Library](https://developer.android.com/topic/libraries/data-binding/), [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/) and [Glide](https://github.com/bumptech/glide).
<br/>
<br/>
I tried to follow all the official [Material Design](https://material.io/design/) guidelines.
